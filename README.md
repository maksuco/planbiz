# PlanBiz

Private package to handle Acccounts plans with stripe and payu payments for Business and not users in Laravel, requires many configurations, dont use

# Installation

You can install the package via composer:
```php
composer require maksuco/PlanBiz
```
This Package works with auto discovery in Laravel +5.5, but is compatible with older versions

## Migrations
```php
php artisan migrate
```

### Publish plan files and laravel-mail components
```
php artisan vendor:publish
```

### PayU Config
```php
'payu' => [
  'apiKey' => 'apiKey',
  'apiLogin' => 'apiLogin',
  'merchantId' => 'merchantId',
  'accountId' => 'accountId',
  'responseUrl' => '/thanks',
  'confirmationUrl' => '/api/payupayment',
  'test' => 0, //1 for true
],
```



# Usage

### Checkout Process Accounts

From Blade or controller
```php
PlanBiz::plan_checkout_post($biz_id,$total);
app('\Maksuco\PlanBiz\Http\Controllers\PlanBizController')->plan_checkout_post($biz_id,$total);
```

From routes
```
Route::post('checkout', '\Maksuco\PlanBiz\Http\Controllers\PlanBizController@plan_checkout_post');
```

### Cancel Plan

### Change Plan (Downgrade,Upgrade)
```php
PlanBiz::change_plan();
app('\Maksuco\PlanBiz\Http\Controllers\PlanBizController')->change_plan($biz_id,$new_plan_id);
```

### Suspend Plan

### Add days to Plan

### Cancel Plan
```php
PlanBiz::cancel($biz_id);
```

### Delete Biz
```php
PlanBiz::delete_biz($biz_id);
```


## CRON

### Suspend Accounts
Use cron to shedule a suspended accounts each day
```php
php artisan planbiz:process
```


## Views
By default the package publishes views/planbiz a few views for emails and order pages, you can modify this pages


