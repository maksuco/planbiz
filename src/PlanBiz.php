<?php

namespace Maksuco\PlanBiz;

class PlanBiz
{
	
	function test($data) {
		//return 'hola';
	  return app('\Maksuco\PlanBiz\Http\Controllers\PlanBizController')->test($data);
	}
	
	function pay($biz,$total) {
	  return app('\Maksuco\PlanBiz\Http\Controllers\PlanBizController')->pay($biz_id,$total);
	}
	
	function change($biz_id,$new_plan_id) {
	  return app('\Maksuco\PlanBiz\Http\Controllers\ChangeController')->change($biz_id,$new_plan_id);
	}
	
	function upgrade($biz_id,$new_plan_id) {
	  return app('\Maksuco\PlanBiz\Http\Controllers\ChangeController')->upgrade($biz_id,$new_plan_id);
	}
	
	function downgrade($biz_id,$new_plan_id) {
	  return app('\Maksuco\PlanBiz\Http\Controllers\ChangeController')->downgrade($biz_id,$new_plan_id);
	}
	
	function cancel($biz_id,$biz_api,$user_id) {
	  return app('\Maksuco\PlanBiz\Http\Controllers\PlanBizController')->cancel($biz_id,$biz_api,$user_id);
	}
	
	function adddays($biz_id,$biz_api) {
	  return app('\Maksuco\PlanBiz\Http\Controllers\PlanBizController')->adddays($biz_id,$biz_api,$days);
	}
	
	
}
