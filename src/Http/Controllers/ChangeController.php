<?php

namespace Maksuco\PlanBiz\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Auth;
use Carbon;
use StdClass;
use Stripe\Stripe;
use Stripe\Charge;

use DB;
use App\Biz;
use App\User;
use App\Plans;
use App\PlanPayments;
use App\PlanCoupons;

use Maksuco\PlanBiz\Mail\PlanSuspend;

class ChangeController extends Controller
{

	public function change($biz_id,$new_plan_id)
	{
		$today = Carbon::today();
		$biz = Biz::where('id', $biz_id)->first();
		$new_plan = Plan::where('id', $new_plan_id)->first();

		//CHECK IF DOWNGRADE
		if($biz->Planx->price < $new_plan->price){
			$results = $this->downgrade($biz->id,$new_plan_id);
			$biz->plan_id = $new_plan->id;
			$biz->plan_credit = $results['credit'];
			$biz->save();
			return back()->with('success', __('downgraded'));
		}
		
		$results = $this->upgrade($biz,$new_plan_id);

		//BIZ UPDATE
		$biz->plan_id = $new_plan->id;
		$biz->save();
		return back()->with('success', __('upgraded'));

	}


	public function upgrade($biz_id,$new_plan_id)
	{
		$biz = Biz::where('id', $biz_id)->first();
		$new_plan = Plan::where('id', $new_plan_id)->first();
		$days = Carbon::parse($plan_next_payment)->diffInDays();

		//if more than 30 days
		$total = ($days > 30)? ($new_plan / 365) - ($biz->Planx->price / 365) : 0;
		return ['price'=>round($total * $days),'days'=>$days];

	}


	public function downgrade($biz_id,$new_plan_id)
	{
		$biz = Biz::where('id', $biz_id)->first();
		$new_plan = Plan::where('id', $new_plan_id)->first();
		//Calculate credit if more than 10 days
		$days = Carbon::parse($plan_next_payment)->diffInDays();
		$credit = ($days > 9)? ($biz->Planx->price / 365) - ($new_plan->price / 365) : 0;
		return ['credit'=>round($credit * $days),'days'=>$days];
	}

}