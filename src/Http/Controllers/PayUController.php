<?php

namespace Maksuco\PlanBiz\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class PayUController extends Controller
{
	public function signature($referenceSale,$amount,$currency)
	{
		$signature = "ApiKey~".config('services.payu.merchantId')."~".$referenceSale."~".$amount."~".$currency;
		$signature = md5($signature);
		return $signature;
	}

	public function payu_button_redirect($referenceSale,$amount,$currency,$email,$biz_name)
	{
		$biz = Biz::find('');
		$sign = $this->signature($referenceSale,$amount,$currency);
		$button = 
		'<form name="form" method="post" action="https://sandbox.checkout.payulatam.com/ppp-web-gateway-payu/">
			<input name="merchantId" type="hidden" value="'.config('services.payu.merchantId').'" >
			<input name="accountId" type="hidden" value="'.config('services.payu.accountId').'" >
			<input name="description" type="hidden" value="'.env('APP_NAME').': '.$biz_name.'" >
			<input name="referenceCode" type="hidden" value="TestPayU" >
			<input name="amount" type="hidden" value="'.$amount.'" >
			<input name="currency" type="hidden" value="'.$currency.'" >
			<input name="signature" type="hidden" value="'.$sign.'" >
			<input name="test" type="hidden" value="'.config('services.payu.test').'" >
			<input name="buyerEmail" type="hidden" value="'.$email.'" >
			<input name="responseUrl" type="hidden"  value="'.url(config('services.payu.accountId')).'" >
			<input name="confirmationUrl" type="hidden"  value="'.url(config('services.payu.accountId')).'" >
		</form><script>setTimeout("document.form.submit();",1000);</script>';
		return $button;
	}


	public function payu_charge()
	{
		
		if($_REQUEST['state_pol'] == 4) {


			//TEST IF REAL
			$sign = $this->signature($referenceSale,$amount,$currency."~4");
			if($sign != $_REQUEST['sign']){
				return response()->json('success', 200);
			}

			$plan_payment->reference = $_REQUEST['reference_pol'];
			
			return response()->json('success', 200);
			
		}

		return response()->json('success', 200);
	}


}