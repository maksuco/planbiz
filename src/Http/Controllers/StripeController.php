<?php

namespace Maksuco\PlanBiz\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class StripeController extends Controller
{

	public function stripe_charge($total,$currency,$stripeToken,$biz_name)
	{
		Stripe::setApiKey(config('services.stripe.secret'));
		try {
			Charge::create(array(
					"currency"    => $currency,
					"amount"      => (int)$total * 100,
					"source"      => request()->stripeToken, //$stripeToken funcionara con el request?
					"description" => env('APP_NAME').': '.$biz_name
			));
		} catch(Exception $e) {
			return back()->route('checkout')->with('alert', $e->getMessage());
		}
	}


}