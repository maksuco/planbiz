<?php

namespace Maksuco\PlanBiz\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Auth;
use Carbon;
use StdClass;
use Stripe\Stripe;
use Stripe\Charge;

use DB;
use App\Biz;
use App\User;
use App\Plans;
use App\PlanPayments;
use App\PlanCoupons;

use Maksuco\PlanBiz\Mail\PlanSuspend;

class PlanBizController extends Controller
{

	public function test($data)
	{
		//$biz = Biz::where('id', 1)->first();
		//$plan_payment = PlanPayments::where('biz_id', $biz->id)->orderBy('date', 'desc')->first();
		//$plans = Plans::get()->toArray();
		//Mail::to('mm@maksuco.com')->queue((new PlanSuspend($biz,null))->locale(auth()->user()->lang??app()->getLocale()));
		return 'desde facade si '.$data;
	}

	public function plan_checkout_post($biz_id,$total)
	{
		$today = Carbon::today();
		$biz = Biz::where('id', $biz_id)->first();
		$plan = Plan::where('id', request()->plan_id)->first();

		//IF NEW CLIENT
		$new_biz = ($biz->status == 0 AND empty($biz->plan_next_payment))? true : false;

		//IF TRIAL PERIOD
		if(isset($request->trial_period)){
			$total = 0;
			$plan_next_payment = $this->plan_next_payment($biz,$request->trial_period);
			$payment_method = 'trial';
		}

		//IF COUPON SOMEDAY WE'LL FINISH IT
		//como sabemos q la misma persona no lo usa 10 veces, maybe if status = 0
		if(!empty($request->coupon) AND $total > 0){
			$coupon = PlanCoupon::where('code', $request->coupon)->first();

			if(empty($coupon)){ return back()->with('message',''); }

			//VALIDATE COUPON
			if($coupon->date_start > $today AND $coupon->date_end < $today AND $coupon->uses < $coupon->uses_max){
				$coupon->uses = $coupon->uses + 1;
				$coupon->save();
				$plan_next_payment = $this->plan_next_payment($biz,$coupon->free_days);
				$biz->plan_id = $coupon->plan;
			}
		}

		//IF CREDIT APPLY IT
		if($biz->plan_credit > 0 AND $total > 0){
			if($biz->plan_credit > $total){
				$credit_applied = $total;
				$new_biz_credit = $biz->plan_credit - $total;
				$payment_method = 'credit';
			} else {
				$credit_applied = $biz->plan_credit;
				$new_biz_credit = 0;
			}
			$total = max($total - $biz->plan_credit,0);
		}

		//PROCESS PAYMENT
		if($total > 0){
			//STRIPE OR PAYU
			if(request()->method == 'stripe'){
				Stripe::setApiKey(config('services.stripe.secret'));
				try {
					Charge::create(array(
							"currency"    => 'usd',
							"amount"      => (int)$total * 100,
							"source"      => request()->stripeToken,
							"description" => "TusMenus.com Pago: ".$biz->name
					));
				} catch(Exception $e) {
					return back()->route('checkout')->with('alert', $e->getMessage());
				}
			} elseif(request()->method == 'payu'){
				//PAYU CODE
			}
		}


		//BIZ UPDATE
		$biz->status = 1;
		$biz->plan_id = $plan->id;
		$biz->plan_next_payment = (isset($plan_next_payment)) ?? $this->plan_next_payment($biz,$plan->period);
		$biz->plan_credit = $new_biz_credit;


		//PLAN RECORD
		$plan_payment = new PlanPayment;
		$plan_payment->biz_id = $biz->id;
		$plan_payment->user_id = auth()->user()->id ?? '';
		$plan_payment->status = 1;
		$plan_payment->total = $total;
		$plan_payment->plan_id = $plan->id;
		$plan_payment->plan_name = $plan->name;
		$plan_payment->plan_price = $plan->price;
		$plan_payment->method = $payment_method ?? request()->method;
		$plan_payment->reference = request()->stripeToken ?? '';
		$plan_payment->credit = $credit_applied ?? 0;
		//$plan_payment->coupon = ;
		//$plan_payment->coupon_id = $coupon->id ?? null;
		$plan_payment->date = $today;

		$plan_payment->save();
		$biz->save();

		if($new_biz){
			Mail::to(auth()->user()->email)->queue((new PlanWelcome($biz))->locale(auth()->user()->lang??app()->getLocale()));
		}

		Mail::to(auth()->user()->email)->queue((new PlanInvoice($biz,$plan_payment))->locale(auth()->user()->lang??app()->getLocale()));

		return $plan_payment->id;
	}


	public function plan_next_payment($biz,$period)
	{
		$today = Carbon::today();
		$date_condition = (!empty($biz->plan_next_payment) AND $biz->plan_next_payment > $today)? true : false;

		if($period == 'year'){
			return ($date_condition)? Carbon::parse($biz->plan_next_payment)->addYear() : $today->addYear();
		} elseif($period == 'month'){
			return ($date_condition)? Carbon::parse($biz->plan_next_payment)->addMonth() : $today->addMonth();
		} elseif(is_int($period)){
			return ($date_condition)? Carbon::parse($biz->plan_next_payment)->addDays($period) : $today->addDays($period);
		} else {
			return $today;
		}
	}


}