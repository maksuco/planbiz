@component('mail::message')
# {{__('suspended_biz_title', ['biz'=>$biz->name])}} {{$biz->name}}

##{{__('Hi')}} {{\Helpers::firstname('Michael Mc')}},
Sorry to say but your account has been suspended

@component('mail::panel')
This is the panel content.
@endcomponent


@component('mail::button', ['url' => url('account/'.$biz->slug)])
{{__('pay')}}
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

