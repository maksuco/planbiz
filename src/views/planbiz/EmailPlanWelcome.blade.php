@component('mail::message')
# {{__('welcome_biz_title', ['biz'=>$biz->name])}} {{$biz->name}}

##{{__('Hi')}} {{\Helpers::firstname('Michael Mc')}},

@component('mail::button', ['url' => url('account/'.$biz->slug)])
{{__('enter_biz')}}
@endcomponent

@component('mail::panel')
This is the panel content.
@endcomponent

Thanks,<br>
{{config('app.name')}}
@endcomponent

