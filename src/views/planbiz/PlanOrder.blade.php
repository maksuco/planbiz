
@php
  $plan_data = json_decode($biz->plan_data);
  $loc_count = count($biz->Locationsx);
@endphp

  <section class="row mt-40 mobile-mt-40" id="checkout" v-cloak>
    <figure class="col-md-8 text-left">
      <div class="bg-white p-15 py-40">

      <h3>1. {{__('planbiz::choose_plan')}}:</h3>
      <div class="switch-field mt-15">
        <input class="" type="radio" value="15" id="plan1" v-model="plan_price">
        <label class="" for="plan1"><b>BASICO</b><span>15</span><div v-if="{{$biz->plan}} != 1">Por un Año</div> <div class="btn btn-warning btn-xs" v-if="{{$biz->plan}} == 1">{{__('actual')}}</div></label>
        <input class="" type="radio" value="30" id="plan2" v-model="plan_price">
        <label class="" for="plan2"><b>PREMIUM</b><span>30</span><div v-if="{{$biz->plan}} != 2">Por un Año</div> <div class="btn btn-warning btn-xs rounded-0" v-if="{{$biz->plan}} == 2">{{__('actual')}}</div></label>
        <input class="" type="radio" value="100" id="plan3" v-model="plan_price">
        <label class="" for="plan3"><b>PREMIUM+WEB</b><span>100</span><div v-if="{{$biz->plan}} != 3">Incluye plan premium<br>y sitio web</div> <div class="btn btn-warning btn-xs rounded-0" v-if="{{$biz->plan}} == 3">{{__('actual')}}</div></label>
      </div>
      <div class="font-light font-14 lineh-100">Con el plan +Web te damos una página web (basada en plantillas) que se controla con tusmenus en tu propio .com (Necesitas un hosting y dominio)</div>

      <div class="alert alert-danger mt-2" role="alert" v-if="plan_price == 15 && {{$biz->plan}} > 1">
        <i class="fas fa-exclamation-triangle"></i> Alerta: Tu plan actual es Premium<br>
        <small>Si te pasas a basico dejaras de aparecer entre los primeros en las listas. Si tienes sitio web con nosotros necesitas el plan + Web.</small>
      </div>

      <div class="alert alert-danger mt-2" role="alert" v-if="plan_price < 100 && {{$biz->plan}} == 3">
        <i class="fas fa-exclamation-triangle"></i> Alerta: Tu plan actual incluye Pagina Web<br>
        <small>Si tienes sitio web con nosotros necesitas el plan + Web (Solo importante si usas tusmenus para crear tu sitio web).</small>
      </div>


      <h3 class="mt-50">2. {{__('How_many_locations')}}</h3>
      <div class="font-light font-14 lineh-100">Cuantos locales (sucursales) tiene tu restaurante? {{__('necesitas_borrar_loc')}}<br><b>Tu plan incluye una sucursal gratis, pero cada una adicional tiene un valor de 5usd por año</b></div>

      <div class="input-group mt-3">
          <span class="input-group-btn">
              <button class="btn btn-link btn-minuse" type="button" v-on:click="substract_count"><i class="fas fa-minus" id="substract_countx"></i></button>
          </span>
          <input type="text" class="form-control text-center" maxlength="3" id="loc_count" v-model="loc_count" style="max-width: 100px">
          <span class="input-group-btn">
              <button class="btn btn-link btn-pluss" type="button" v-on:click="add_count"><i class="fas fa-plus" id="add_countx"></i></button>
          </span>
      </div>

      <h3 class="mt-50">3. {{__('planbiz::options')}}</h3>
      <div class="custom-control custom-checkbox mt-3">
        <input type="checkbox" class="custom-control-input" id="extra_support_onetime" v-model="extra_support_onetime">
        <label class="custom-control-label font-20" for="extra_support_onetime"><h5>Ayuda por un mes $30<br><small class="font-light font-14">Te ayudamos a subir fotos, descripciones y tu menú cuando te inscribes y por el primer mes.</small></h5></label>
      </div>
      <div class="custom-control custom-checkbox mt-3">
        <input type="checkbox" class="custom-control-input" id="extra_support_year" v-model="extra_support_year">
        <label class="custom-control-label font-20" for="extra_support_year"><h5>Ayuda por un año $120<br><small class="font-light font-14">Durante un año te ayudamos a mantener tu listado, subiendo promociones, nuevas fotos y todos los cambios que requieras.</small></h5></label>
      </div>

      {{--
      <div class="custom-control custom-checkbox mt-3">
        <input type="checkbox" class="custom-control-input" id="extra_website" v-model="extra_website">
        <label class="custom-control-label font-20" for="extra_website"><h5>Sitio web $100<br><small class="font-light font-14">Te damos una página web (basada en plantillas) que se controla con tusmenus en tu propio .com (Necesitas un hosting y dominio)</small></h5></label>
      </div>
      --}}

      {{--
      <div class="custom-control custom-checkbox mt-3">
        <input type="checkbox" class="custom-control-input" id="extra_website" v-model="extra_website_custom">
        <label class="custom-control-label font-20" for="extra_website_custom"><h5>Website a la medida $500<br><small class="font-light font-14">Te hacemos una página a tu medida con todos los beneficios de Tusmenus</small></h5></label>
      </div>
      --}}

      </div>	
    </figure>

    <figure class="col-md-4 text-left">
    <div class="p-15">
          <h5 class="bold pb-10">{{__('planbiz::includes')}}:</h5>
          <ul class="list-group mt-2">
              <li class="list-group-item d-flex justify-content-between lh-condensed">
                <div><h5 class="my-0">Plan: @{{plan_name}}</h5></div>
                <span class="text-muted">$@{{plan_price}}</span>
              </li>
              <li class="list-group-item d-flex justify-content-between lh-condensed mt-1">
                <div><h5 class="my-0">@{{loc_count}} <div class="d-inline" v-if="loc_count > 1">{{__('Locations')}}</div><div class="d-inline" v-if="loc_count == 1">{{__('Location')}}</div></h5><small class="text-muted">Adicionales: @{{loc_count - 1}} x 5usd</small></div>
                <span class="text-muted">$@{{loc_count_total}}</span>
              </li>
              <li class="list-group-item d-flex justify-content-between lh-condensed mt-1" v-if="extra_support_onetime">
                <div><h6 class="my-0">Soporte una sola vez</h6><small class="text-muted">Asistente por un 30 días</small></div>
                <span class="text-muted">$30</span>
              </li>
              <li class="list-group-item d-flex justify-content-between lh-condensed mt-1" v-if="extra_support_year">
                <div><h6 class="my-0">Soporte por un año</h6><small class="text-muted">Asistente por un año</small></div>
                <span class="text-muted">$120</span>
              </li>
              {{--
              <li class="list-group-item d-flex justify-content-between lh-condensed mt-1" v-if="extra_website">
                <div><h6 class="my-0">Sitio web propio</h6><small class="text-muted"></small></div>
                <span class="text-muted">$100</span>
              </li>
              --}}
              <li class="list-group-item d-flex justify-content-between bg-light" v-if="credit != 0">
                <span>Credito (USD)<br><small class="text-muted">{{__("planbiz::credit_favor_msg")}}</small></span>
                <strong>- $@{{credit}}</strong>
              </li>
              <li class="list-group-item d-flex justify-content-between bg-light">
                <h5>Total (USD)</h5>
                <strong>$@{{total_price}}</strong>
              </li>
            </ul>
            {{--<a :href="'/user/checkout/1/payment?total_price='+total_price+'plan='+plan_name" class="btn btn-primary btn-lg btn-block mt-3">{{__('Continue_checkout')}}</a>--}}

<script src="https://checkout.stripe.com/checkout.js"></script>
<form id="StripeCheckout-form" action="" method="POST">
  @csrf
  <input type="hidden" name="method" v-bind:value="(total_price > 0)?'stripe':'credit'">
  <input type="hidden" name="plan_name" v-bind:value="plan_name">
  <input type="hidden" name="plan_price" v-bind:value="plan_price">
  <input type="hidden" name="loc_count" v-bind:value="loc_count">
  <input type="hidden" name="extra_support_onetime" v-bind:value="extra_support_onetime">
  <input type="hidden" name="extra_support_year" v-bind:value="extra_support_year">
  <input type="hidden" name="extra_website" v-bind:value="extra_website">
  <input type="hidden" name="total" v-bind:value="total_price">
  <input type="hidden" name="credit" v-bind:value="credit">
  <button class="btn btn-primary btn-lg btn-block mt-3 btn-spinner" @click.prevent="stripe_checkout" v-if="(total_price > 0)">{{__("planbiz::pay_card")}}</button>
  <button type="submit" class="btn btn-primary btn-lg btn-block mt-3 btn-spinner" v-if="(total_price < 1)">{{__("planbiz::use_credit")}}</button>
</form>

{{--
<form method="post" action="https://sandbox.checkout.payulatam.com/ppp-web-gateway-payu/" class="mt-1" v-if="(total_price < 1)">
<input name="merchantId" type="hidden" value="508029">
<input name="accountId" type="hidden" value="512321">
<input name="signature" type="hidden" value="7ee7cf808ce6a39b17481c54f2c57acc">
<input name="description" type="hidden" value="Pago: {{$biz->name}}">
<input name="referenceCode" type="hidden" value="TestPayU">
<input name="amount" type="hidden" v-bind:value="plan_price">
<input name="currency" type="hidden" value="USD">
<input name="test" type="hidden" value="1">
<input name="buyerEmail" type="hidden" value="{{auth()->user()->email}}">
<input name="responseUrl" type="hidden" value="{{url('/account/'.$biz->id.'/dashboard')}}">
<input name="confirmationUrl" type="hidden" value="{{url('/account/'.$biz->id.'/checkout')}}">
<button type="submit" class="btn btn-primary btn-lg btn-block mt-3 btn-spinner">Pagar con PayU</button>
</form>
--}}
              
      @if(!empty($biz->payment_next))
        @if(Date::parse($biz->payment_next) > Date::today())
          <h5 class="bold mt-20">{{__("planbiz::pay_note_before, ['date'=>Date::parse($biz->payment_next)->format('M j Y')]")}}</h5>
        @elseif(Date::parse($biz->payment_next) == Date::today())
          <h5 class="bold mt-20">{{__("planbiz::pay_note_today, ['date'=>Date::parse($biz->payment_next)->format('M j')]")}}</h5>
        @else
          <h5 class="bold mt-20">{{__("planbiz::pay_note_after, ['date'=>Date::parse($biz->payment_next)->format('F j')]")}}</h5>
        @endif
      @endif

      </div>	
    </figure>      
  </section>

@push('scripts')

<script>
  var app = new Vue({
    el: '#checkout',
    data: {
      plan_price: '{{$plan_data->price??15}}',
      plan_name: '{{$plan_data->name??'Basico'}}',
      loc_count: {{$loc_count}},
      loc_count_total: {{$loc_count}},
      extra_support_onetime: false,
      extra_support_year: {{(isset($plan_data->support_year))?'true':'false'}},
      extra_website: {{(isset($plan_data->website))?'true':'false'}},
      credit: {!!(isset($plan_data->website))?$plan_data->credit:"''"!!}
    },
    created: {
      total_price: 'total_price'
    },
    watch: {
      plan_price: 'plan_change',
    },
    methods: {
      stripe_checkout: function(){
        var token = function(res){
          var stripeToken = $('<input type="hidden" name="stripeToken" />').val(res.id);
          var email = $('<input type="hidden" name="stripeEmail" />').val(res.email);
          setTimeout(function(){
            $('#StripeCheckout-form').append(stripeToken).append(email).submit();
          }, 1500);
        };
        StripeCheckout.open({
          key: '{{env('STRIPE_KEY')}}',
          currency: 'usd',
          name: 'TusMenus.com',
          image: 'https://s3.amazonaws.com/stripe-uploads/acct_104Ez24yw5kHeXJCmerchant-icon-1457910812053-icon-152.png',
          description: 'Pago: {!!$biz->name!!}',
          token: token,
          amount: this.total_price * 100,
          locale: '{{__('es')}}',
          code: 'false',
        });
      },
      plan_change: function(event){
        if(event == 15) {
          this.plan_name = 'Basico';
        } else {
          this.plan_name = (event==30)?'Premium':'Premium + Web';
        }
        alert(plan_name);
      },
      add_count: function(){
        if(this.loc_count < 50) {
          this.loc_count += 1;
        }
      },
      substract_count: function(){
        if(this.loc_count != {{$loc_count}}) {
          this.loc_count -= 1;
        } else if(this.loc_count != 1) {
          alert('{{__('necesitas_borrar_loc')}}');
        }
      }
    },
    computed: {
      total_price: function () {
        if(this.loc_count>1){
          this.loc_count_total = (parseFloat(this.loc_count) - 1 )*5
        } else {
          this.loc_count_total= 0
        }
        let total = parseFloat(this.plan_price) + this.loc_count_total;
        if(this.extra_support_onetime)
          total = total + 30
        if(this.extra_support_year) 
          total = total + 120
        if(this.extra_website) 
          total = total + 100

        if(this.credit > total){
          return 0;
        } else {
          return total - this.credit;
        }
        }
      }
  })
</script>
@endpush