@component('mail::message')
# {{__('planinvoice_biz_title', ['biz'=>$biz->name])}} {{$biz->name}}

##{{__('Hi')}} {{\Helpers::firstname('Michael Mc')}},
{{__('planinvoice_biz_desc', ['biz'=>$biz->name])}}

@component('mail::button', ['url' => url('account/'.$biz->slug)])
{{__('enter_biz')}}
@endcomponent

@component('mail::panel')
This is the panel content.
@endcomponent

@component('mail::table')
	| | Price |
	| :-------- |--------:|
	
	| Plan: {{$plan_payment->plan_name}} | ${{$plan_payment->plan_price}} |

	@if($plan_payment->credit > 0)
	| Credit | - ${{$plan_payment->credit}} |
	@endif

	@if($plan_payment->coupon > 0)
	| Coupon | - ${{$plan_payment->credit}} |
	@endif

	| Total (USD) | ${{$plan_payment->total - $plan_payment->credit}} |
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

