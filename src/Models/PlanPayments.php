<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanPayments extends Model
{
    protected $table = 'plan_payments';
}
