<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Biz extends Model
{
    protected $table = 'biz';

    public function Planx() {
        return $this->belongsTo('App\Plans', 'plan_id', 'id');
    }
}
