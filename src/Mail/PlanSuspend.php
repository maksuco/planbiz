<?php

namespace Maksuco\PlanBiz\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use Jenssegers\Date\Date;

class PlanSuspend extends Mailable
{
    use Queueable, SerializesModels;

    public $biz;
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($biz,$user)
    {
        $this->biz = $biz;
        $this->user = $user;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        Date::setLocale($this->user->lang ?? 'es');
        return $this->subject(__('plan_suspend_subject', ['biz_name'=>$this->biz->name]))->markdown('planbiz.PlanSuspend');
    }
}
