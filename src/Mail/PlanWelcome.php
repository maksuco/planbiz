<?php

namespace Maksuco\PlanBiz\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PlanWelcome extends Mailable
{
    use Queueable, SerializesModels;

    public $biz;
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($biz,$user)
    {
        $this->biz = $biz;
        $this->user = $user;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        Date::setLocale($this->user->lang ?? app()->getLocale());
        return $this->subject(__('plan_welcome_subject', ['biz_name'=>$this->biz->name]))->markdown('planbiz.PlanWelcome');
    }
}
