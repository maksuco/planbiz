<?php

namespace Maksuco\PlanBiz\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;

class PlanInvoice extends Mailable
{
    use Queueable, SerializesModels;

    public $biz;
    public $user;
    public $plan_payment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($biz,$user,$plan_payment)
    {
      $this->middleware('auth');
      $this->biz = $biz;
      $this->user = $user;
      $this->plan_payment = $plan_payment;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        Date::setLocale($this->user->lang ?? app()->getLocale());
        return $this->subject(__('plan_plansuspend_subject', ['biz_name'=>$this->biz->name]))->markdown('subscription.PlanSuspend');
    }
}
