<?php

namespace Maksuco\PlanBiz;

use Illuminate\Support\ServiceProvider;

class PlanBizServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      $this->loadRoutesFrom(__DIR__.'/routes.php');
      $this->loadTranslationsFrom(__DIR__.'/lang', 'planbiz'); //trans('planbiz::es.price'); __('planbiz::price')
      $this->loadViewsFrom(__DIR__ . '/views', 'index');
      $this->loadViewsFrom(app_path('vendors/maksuco/planbiz/src/views'), 'planbiz');
      $this->mergeConfigFrom(
          __DIR__ . '/config/planbiz.php', 'planbiz'
      );

      $this->publishes([
          __DIR__ . '/config/planbiz.php' => config_path('planbiz.php'),
          __DIR__ . '/views/planbiz/' => resource_path('views/planbiz'),
          __DIR__ . '/Models' => app_path(''),
          //__DIR__ . '/Mail' => app_path('Mail')
      ]);

      $this->loadMigrationsFrom(__DIR__.'/database/migrations');

      if ($this->app->runningInConsole()) {
        $this->commands([
          Commands\PlanBizCommand::class,
        ]);
      }
    }


    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      $this->app->bind('maksuco-planbiz', function () {
        return new PlanBiz();
      });
    }
}
