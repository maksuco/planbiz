<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanBizTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
			Schema::create('plans', function (Blueprint $table) {
					$table->increments('id');
					$table->smallInteger('status')->default('0');
					$table->integer('group');
					$table->string('name');
					$table->string('namex');
					$table->text('desc');
					$table->text('descx'); //will use the lang files
					$table->string('period'); //month or year
					$table->integer('price');
					$table->integer('tax')->nullable(); //just in case
					$table->string('currency')->default('usd');
			});

			//Fill plans with first plan info
			DB::table('plans')->insert([
				'name' => 'Basic',
				'namex' => 'plan_1',
				'group' => 1,
				'desc' => 'Made for freelancers',
				'descx' => 'plan_1_desc',
				'period' => 'year',
				'price' => 5
			]);
			

			Schema::create('plan_payments', function (Blueprint $table) {
					$table->increments('id');
					$table->smallInteger('status')->default('0');
					$table->integer('biz_id');
					$table->integer('user_id')->nullable();
					$table->integer('plan_id');
					$table->string('plan_name');
					$table->integer('plan_price');
					$table->integer('total_paid');
					$table->string('currency');
					$table->string('method')->nullable();
					$table->string('reference')->nullable();
					$table->integer('coupon')->nullable();
					$table->integer('coupon_id')->nullable();
					$table->integer('credit')->nullable();
					$table->dateTime('date');
					$table->text('notes');
			});

			Schema::create('plan_coupons', function (Blueprint $table) {
					$table->increments('id');
					$table->smallInteger('status')->default('0');
					$table->string('name');
					$table->string('code');
					$table->integer('uses');
					$table->integer('uses_max');
					$table->integer('free_days');
					$table->integer('plan');
					$table->dateTime('date_start');
					$table->dateTime('date_end');
					$table->text('response');
					$table->timestamps();
			});

			if(!Schema::hasTable('biz')) {
				Schema::create('biz', function (Blueprint $table) {
						$table->increments('id');
						$table->smallInteger('status')->default('0');
						$table->string('name');
						$table->string('slug');
						$table->timestamps();
				});
			}
			Schema::table('biz', function (Blueprint $table) {
				$table->integer('plan_id')->nullable();
				$table->date('plan_next_payment')->nullable();
				$table->text('plan_data')->nullable(); //this is saved in the app with the custom fields
				$table->text('plan_credit')->nullable();
				$table->text('plan_used_coupons')->nullable();
			});

			//if(!file_exists(app_path().'Biz.php')){
			//	Artisan::call("make:model", ['name' => 'Biz']);
			//}
			//Artisan::call("make:model", ['name' => 'Plans']);
			//Artisan::call("make:model", ['name' => 'PlanPayments']);
			//Artisan::call("make:model", ['name' => 'PlanCoupons']);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
			Schema::drop('plans');
			Schema::drop('plan_payments');
			Schema::drop('plan_coupons');
			if(Schema::hasTable('biz')) {
				Schema::table('biz', function (Blueprint $table) {
					$table->dropColumn('plan_id');
					$table->dropColumn('plan_next_payment');
					$table->dropColumn('plan_data');
					$table->dropColumn('plan_credit');
				});
			}
    }
}