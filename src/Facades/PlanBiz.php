<?php

namespace Maksuco\PlanBiz\Facades;

use Illuminate\Support\Facades\Facade;

class Subscription extends Facade
{
    protected static function getFacadeAccessor()
    {
        return ('maksuco-subscription');
    }
}
