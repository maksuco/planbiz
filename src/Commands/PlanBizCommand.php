<?php

namespace Maksuco\PlanBiz\Commands;

use Illuminate\Console\Command;
//use Jenssegers\Date\Date;
use Carbon\Carbon;
//use App\Mail\SendInvoice;
use App\Biz;

class PlanBizCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'planbiz:process';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = Carbon::today();
        $yesterday = Carbon::today()->subDay();
        $five = Carbon::today()->subDays(5);
        $ten = Carbon::today()->subDays(10);
        $halfmonth = Carbon::today()->subDays(15);
        $five_plus = Carbon::today()->addDays(5);
        $halfmonth_plus = Carbon::today()->addDays(15);
        $this->info('Starting '.$today);
 
        //NOTIFY BEFORE 5, 15 DAYS
        $this->info('NOTIFY BEFORE');
        $accounts = Biz::whereIn('plan_next_payment', [$five_plus,$halfmonth_plus])->get();
        $email_content = __('account_email_upcoming_payment_reminder');
        foreach($accounts as $account) {
            $this->line($account->name.' - '.$account->id);
            $subject = $title = __('upcoming_payment_reminder', ['name' => $account->name]);
            Mail::to($client->email)->queue(new AccountNotification($account));
        }
     
        //NOTIFY AFTER 1, 5, 10, 15 DAYS
        $this->info('NOTIFY AFTER');
        $accounts = Biz::whereIn('plan_next_payment', [$yesterday,$five,$ten,$halfmonth])->get();
        $email_content = __('account_suspended_emailcontent');
        $link_title = __('Details');
        $link = 'user/accounts';
        foreach($accounts as $account) {
            $this->line($account->name.' - '.$account->id);
            $client->status = 3;
            $client->update();
            $subject = $title = __('account_suspended', ['name' => $client->name]);
            $queue = (new AccountNotification($client,NULL,$subject,$title,$email_content,$link,$link_title));
            Mail::to($client->email)->queue($queue);
            echo $client->id.', ';
        }
     
        //SUSPEND ACCOUNTS
        $this->info('SUSPEND ACCOUNTS');
        $suspended_accounts = Biz::where('plan_next_payment', $yesterday)->get();
        foreach($suspended_accounts as $accounts) {
            $this->line($account->name.' - '.$account->id);
            $client->status = 3;
            $client->update();
        }
    }
}
